#
# Config
#

general {
    border_size = 2
    col.active_border = rgb(7040ff) rgb(05b5ff) 45deg
    col.inactive_border = rgb(242424)

    gaps_in = 5
    gaps_out = 10

    layout = dwindle
}

decoration {
    shadow_range = 12
    shadow_render_power = 2
    col.shadow = rgba(000000a0)

    rounding = 5

    blur {
        noise = 0
        contrast = 1
        brightness = 1
    }
}

animations {
    enabled = yes

    animation = windows, 1, 5, default
    animation = windowsOut, 1, 5, default, popin 80%
    animation = border, 1, 7, default
    animation = fade, 1, 3, default
    animation = workspaces, 1, 4, default
}

input {
    follow_mouse = 2
    float_switch_override_focus = 0

    accel_profile = flat
    scroll_method = 2fg

    touchpad {
        disable_while_typing = false
        natural_scroll = true
        drag_lock = true
    }
}

dwindle {
    pseudotile = true
    preserve_split = true
}

gestures {
    workspace_swipe = true
    workspace_swipe_fingers = true
}

misc {
    vrr = 1
    focus_on_activate = true
    disable_hyprland_logo = true
    disable_splash_rendering = true
    force_default_wallpaper = 0
}

xwayland {
  force_zero_scaling = true
}

#
# Keybinds
#

$mod = SUPER

# misc

bind = $mod, H, exec, toggle-rofi -theme ~/.config/rofi/window-theme.rasi -show window
bind = $mod, V, exec, toggle-rofi -theme ~/.config/rofi/theme.rasi -show drun
bind = $mod, C, killactive
bind = $mod, X, exec, loginctl lock-session
bind = $mod, G, exec, send-clipboard
bind = $mod, BackSpace, exec, kill $(hyprctl activewindow | grep -oP '(?<=pid: ).*')
bind = CONTROLALT, Delete, exec, loginctl kill-session $XDG_SESSION_ID

# window movement

bind = $mod, W, movewindow, u
bind = $mod, A, movewindow, l
bind = $mod, S, movewindow, d
bind = $mod, D, movewindow, r
bind = $mod, M, movewindow, l
bind = $mod, COMMA, movewindow, r
bind = $mod, E, togglesplit

# workspace movement

bind = $mod, J, workspace, m-1
bind = $mod, K, workspace, m+1
bind = $mod, mouse_up, workspace, m-1
bind = $mod, mouse_down, workspace, m+1
bind = $mod, F, workspace, empty

# cross-workspace window movement

bind = $mod, Space, submap, move
submap = move

bind = $mod, F, movetoworkspace, empty
bind = $mod, J, movetoworkspace, m-1
bind = $mod, K, movetoworkspace, m+1
bind = $mod, mouse_up, movetoworkspace, m-1
bind = $mod, mouse_down, movetoworkspace, m+1

bind = $mod, F, submap, reset
bind = $mod, Space, submap, reset
bind = , Escape, submap, reset
bind = , mouse:272, submap, reset
bind = , mouse:273, submap, reset

submap = reset

# mouse

bindm = $mod, mouse:272, movewindow
bindm = $mod, mouse:273, resizewindow

# media keys

bind = , XF86AudioRaiseVolume, exec, volumectl -u up
bind = , XF86AudioLowerVolume, exec, volumectl -u down
bind = , XF86AudioMute, exec, volumectl toggle-mute
bind = , XF86AudioMicMute, exec, volumectl -m toggle-mute
bind = , XF86MonBrightnessUp, exec, lightctl up
bind = , XF86MonBrightnessDown, exec, lightctl down
bind = , Print, exec, hyprshot -m region

#
# Window rules
#

layerrule = blur, rofi
layerrule = ignorezero, rofi

windowrulev2 = size 960 540, class:(deadbeef), title:(Search)
windowrulev2 = center, class: (deadbeef), title: (Search)

windowrulev2 = fakefullscreen, class:^(code-url-handler)$
windowrulev2 = float, class:^(code)$

#
# Autostart
#

source=~/.config/hypr/device.conf

exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP QT_QPA_PLATFORMTHEME
exec-once = rm -f ~/.config/deadbeef/running

exec-once = xremap ~/.config/xremap.yml
exec-once = import-settings
exec-once = xrdb ~/.Xresources
exec-once = xsettingsd

exec-once = swayidle -w
exec-once = sh -c "while true; do hyprpaper; done"
exec-once = sh -c "while true; do dolphin --daemon; done"
exec-once = wl-clip-persist --clipboard regular
exec-once = /usr/lib/polkit-kde-authentication-agent-1
exec-once = swaync -s ~/.config/swaync/style.css
exec-once = waybar -s ~/.config/waybar/style.css
exec-once = avizo-service

exec-once = dex -a
exec-once = ~/.config/autostart/autostart
