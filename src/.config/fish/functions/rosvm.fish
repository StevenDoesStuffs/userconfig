function rosvm -a action
    set -x DBX_CONTAINER_MANAGER podman
    if test "$action" = create
        podman create \
            --hostname rosvm.$(hostnamectl hostname) \
            --label manager=distrobox \
            --name rosvm \
            --user root:root \
            --ulimit host \
            --network host \
            --userns keep-id \
            --privileged \
            --env SHELL=zsh \
            --env DISTROBOX_HOST_HOME="$HOME" \
            --env HOME="$HOME" \
            --volume "$HOME/rosvm:$HOME:rslave" \
            --volume /:/run/host:rslave \
            --volume /etc/hosts:/etc/hosts:ro \
            --volume /etc/resolv.conf:/etc/resolv.conf:ro \
            --volume /usr/bin/distrobox-init:/usr/bin/entrypoint:ro \
            --volume /usr/local/bin/distrobox-host-exec-fake:/usr/bin/distrobox-host-exec:ro \
            --entrypoint /usr/bin/entrypoint \
            ubuntu:20.04 \
            --verbose \
            --name $USER \
            --user 1000 \
            --group 1000 \
            --home "$HOME" \
            --init 0 \
            --nvidia 1
        distrobox-generate-entry rosvm
    else if test "$action" = enter
        distrobox enter rosvm -nw
    else if test "$action" = destroy
        distrobox rm rosvm --force
    else
        echo "Invalid action"
        return -1
    end
end
