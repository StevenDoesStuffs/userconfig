set -gx SSH_AUTH_SOCK (gpgconf --list-dirs agent-ssh-socket)
set -gx EDITOR micro
set -gx GOPATH ~/.go
set -gx PNPM_HOME "/home/stevendoesstuffs/.local/share/pnpm"
fish_add_path -gm ~/.local/share/pnpm
fish_add_path -gm ~/.local/bin
fish_add_path -gm ~/.cargo/bin
fish_add_path -gm ~/.yarn/bin
fish_add_path -gm ~/.go/bin
