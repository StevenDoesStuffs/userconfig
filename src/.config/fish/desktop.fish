set -gx EDITOR code-wait

# xdg

set -gx XDG_SESSION_DESKTOP Hyprland
set -gx XDG_SESSION_TYPE wayland
set -gx XDG_MENU_PREFIX arch-

# gui toolkits

set -gx CLUTTER_BACKEND wayland
set -gx GDK_BACKEND wayland
set -gx QT_QPA_PLATFORM wayland

# theming

set -gx PLASMA_USE_QT_SCALING 1
set -gx QT_AUTO_SCREEN_SCALE_FACTOR 1
set -gx QT_WAYLAND_DISABLE_WINDOWDECORATION 1
set -gx QT_QPA_PLATFORMTHEME qt5ct
set -gx HYPRCURSOR_THEME capitaine-cursors
set -gx HYPRCURSOR_SIZE 36
set -gx XCURSOR_THEME $HYPRCURSOR_THEME
set -gx XCURSOR_SIZE 32

# applications

set -gx MOZ_ENABLE_WAYLAND 1
set -gx SDL_VIDEODRIVER wayland
set -gx WLR_DRM_NO_ATOMIC 0
set -gx _JAVA_AWT_WM_NONEREPARENTING 1
set -gx LIBSEAT_BACKEND logind

source ~/.config/fish/device/desktop.fish
