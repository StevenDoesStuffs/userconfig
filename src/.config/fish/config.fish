if status is-login
    source ~/.config/fish/variables.fish
    bass "source /etc/profile"
end

if status is-interactive
    if string match -q "xterm*" "$TERM"
        bind \e\[3\;5~ kill-word
        bind \b backward-kill-word
    end

    alias ls="eza"
    alias cat="bat --paging=never"

    set -x GPG_TTY (tty)

    if status is-login; and test "$START_DESKTOP" = 1
        source ~/.config/fish/desktop.fish
        Hyprland > /tmp/hyprland.log
    end
    eval (starship init fish)
end
