/*******************************************************************************
 * MACOS SPOTLIGHT LIKE DARK THEME FOR ROFI
 * User                 : LR-Tech
 * Theme Repo           : https://github.com/lr-tech/rofi-themes-collection
 *******************************************************************************/

* {
    font:   "Montserrat 12";

    bg0:    #141414a0;
    bg1:    #66666680;
    bg2:    #4079ffE6;

    fg0:    #d9d9d9;
    fg1:    #FFFFFF;
    fg2:    #d9d9d980;

    background-color:   transparent;
    text-color:         @fg0;

    margin:     0;
    padding:    0;
    spacing:    0;
}

window {
    background-color:   @bg0;

    location:       center;
    width:          640;

    border: 2px;
    border-radius: 5px;
    border-color: #4079ff;
}

inputbar {
    font:       "Montserrat 18";
    padding:    12px;
    spacing:    12px;
    children:   [ icon-search, entry ];
}

icon-search {
    expand:     false;
    filename:   "edit-find";
    size: 28px;
    padding: 2px;
}

icon-search, entry, element-icon, element-text {
    vertical-align: 0.5;
}

entry {
    font:   inherit;

    placeholder         : "Search";
    placeholder-color   : @fg2;
}

message {
    border:             2px 0 0;
    border-color:       @bg1;
    background-color:   @bg1;
}

textbox {
    padding:    8px 24px;
}

listview {
    lines:      10;
    columns:    1;

    fixed-height:   true;
    border:         1px 0 0;
    border-color:   @bg1;
}

element {
    padding:            8px 16px;
    spacing:            16px;
    background-color:   transparent;
}

element normal active {
    text-color: @bg2;
}

element selected normal, element selected active {
    background-color:   @bg2;
    text-color:         @fg1;
}

element-icon {
    size:   1em;
}

element-text {
    text-color: inherit;
}
